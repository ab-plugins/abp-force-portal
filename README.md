# ABP Force Portal

This plugin redirects users who come (back) to index page and didn't visit the forum since a delay (fixed by admin) to the portal page.

Guests and members can have different delay until be redirected.

# Upgrade
* Deactivate the plugin from ACP
* Install the plugin

# Installation
* Copy the UPLOAD contents to the forum root
* Activate plugin in ACP

# Settings
* portal page (must be a MyBB page)
* delay for guest (in hours)
* delay for members (in hours)
* groups excluded from redirection

# Languages
english, french