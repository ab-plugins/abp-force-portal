<?php
/**
 * ABP Force Portal
 * By CrazyCat <http://ab-plugin.cc>
 */

if(!defined("IN_MYBB"))
	die("Direct initialization of this file is not allowed.<br />Please make sure IN_MYBB is defined.");

// Unique constant used as codename
define('CN_ABPFP', str_replace('.php', '', basename(__FILE__)));

$plugins->add_hook("global_start", "abp_forceportal_run");

function abp_forceportal_info()
{
	global $lang;
	$lang->load(CN_ABPFP);
	
	return array(
		"name"			=> $lang->forceportal_name,
		"description"	=> $lang->forceportal_desc,
		"website"		=> "http://ab-plugin.cc/ABP-Force-Portal-t-14.html",
		"author"		=> "CrazyCat",
		"authorsite"	=> "http://ab-plugin.cc",
		"version"		=> "1.7.1",
		"compatibility" => "18*",
		"codename"	=> CN_ABPFP
	);
}

function abp_forceportal_install()
{
	global $db, $lang;
	$lang->load(CN_ABPFP);

	$settinggroups = array(
		'name' => CN_ABPFP,
		'title' => $lang->forceportal_name,
		'description' => $lang->forceportal_setting_desc,
		'disporder' => 1,
		'isdefault' => 0,
	);
	
	$db->insert_query('settinggroups', $settinggroups);
	$gid = $db->insert_id();
	
	$settings[] = array(
		'name' => CN_ABPFP.'_url',
		'title' => $lang->forceportal_url_title,
		'description' => $lang->forceportal_url_desc,
		'optionscode' => 'text',
		'value' => 'portal.php'
	);
	$settings[] = array(
		'name' => CN_ABPFP.'_time',
		'title' => $lang->forceportal_time_title,
		'description' => $lang->forceportal_time_desc,
		'optionscode' => 'numeric',
		'value' => '24'
	);
	$settings[] = array(
		'name' => CN_ABPFP.'_utime',
		'title' => $lang->forceportal_utime_title,
		'description' => $lang->forceportal_utime_desc,
		'optionscode' => 'numeric',
		'value' => '72'
	);
	$settings[] = array(
		'name' => CN_ABPFP.'_exclude',
		'title' => $lang->forceportal_exclude_title,
		'description' => $lang->forecportal_exclude_desc,
		'optionscode' => 'groupselect',
		'value' => ''
	);

	foreach($settings as $i => $setting)
	{
		$insert = array(
			'name' => $db->escape_string($setting['name']),
			'title' => $db->escape_string($setting['title']),
			'description' => $db->escape_string($setting['description']),
			'optionscode' => $db->escape_string($setting['optionscode']),
			'value' => $db->escape_string($setting['value']),
			'disporder' => $i,
			'gid' => $gid,
		);
		$db->insert_query("settings", $insert);
	}
	rebuild_settings();
}


function abp_forceportal_uninstall()
{
	global $db;
	$db->delete_query("settings", "name LIKE '".CN_ABPFP."_%'");
	$db->delete_query("settinggroups", "name = '".CN_ABPFP."'");
	rebuild_settings();
}

function abp_forceportal_is_installed()
{
	global $db;
	$installtest = $db->simple_select("settinggroups", "count(*) as nb", "name = '".CN_ABPFP."'");
	$nb = $db->fetch_field($installtest, 'nb');
	if ($nb > 0)
	{
		return true;
	}
	return false;
}

function abp_forceportal_run()
{
	global $mybb;
	$redirect = false;
	if (isset($mybb->user['uid']) && intval($mybb->user['uid'])>0) {
		$expire = intval($mybb->settings['abp_forceportal_utime']) * 60 * 60;
	} else {
		$expire = intval($mybb->settings['abp_forceportal_time']) * 60 * 60;
	}
	if (!defined('THIS_SCRIPT') || (THIS_SCRIPT == 'index.php'))
	{
		if (!isset($mybb->cookies[CN_ABPFP]))
		{
			$redirect = true;
		}
	}
	if (trim($mybb->settings['abp_forceportal_exclude'])!='')
	{
		$excluded = explode(',', $mybb->settings['abp_forceportal_exclude']);
		$ugroup = explode(',', $mybb->user['additionalgroups']);
		$ugroup[] = $mybb->user['usergroup'];
		if (count(array_intersect($excluded, $ugroup))>0)
		{
			$redirect = false;
		}
		
	}
	my_setcookie(CN_ABPFP, true, $expire);
	if ($redirect === true)
	{
		header('Location: '.$mybb->settings['abp_forceportal_url']);
	}
}