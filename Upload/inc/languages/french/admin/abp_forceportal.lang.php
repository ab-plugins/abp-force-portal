<?php

$l['forceportal_name'] = "APB Force Portal";
$l['forceportal_desc'] = "Redirige sur le portail les utilisateurs arrivants sur l'index et n'étant pas venus depuis un certain temps";
$l['forceportal_setting_desc'] = "Réglages pour APB Force Portal";
$l['forceportal_url_title'] = "Page portail";
$l['forceportal_url_desc'] = "La page de portail (<i>défaut: portal.php</i>)";
$l['forceportal_time_title'] = "Durée pour les visiteurs";
$l['forceportal_time_desc'] = "Durée (en heures) après laquelle un visiteur sera redirigé.<br />Mettez 0 pour désactiver la fonction";
$l['forceportal_utime_title'] = "Durée pour les membres";
$l['forceportal_utime_desc'] = "Durée (en heures) après laquelle un membre sera redirigé.<br />Mettez 0 pour désactiver la fonction";
$l['forceportal_exclude_title'] = "Groupes non-affectés";
$l['forceportal_exclude_desc'] = "Choisissez quels groupes ne seront pas redirigés vers le portail";