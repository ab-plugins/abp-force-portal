<?php

$l['forceportal_name'] = "APB Force Portal";
$l['forceportal_desc'] = "Redirects the index to the portal if the last visit was more than the delay you choose";
$l['forceportal_setting_desc'] = "Settings for APB Force Portal";
$l['forceportal_url_title'] = "Portal page";
$l['forceportal_url_desc'] = "The page you use as portal (<i>default: portal.php</i>)";
$l['forceportal_time_title'] = "Delay for guest";
$l['forceportal_time_desc'] = "Time (in hours) after wich guest will be redirected.<br />Set to 0 to disable.";
$l['forceportal_utime_title'] = "Delay for identified users";
$l['forceportal_utime_desc'] = "Time (in hours) after wich identified users will be redirected.<br />Set to 0 to disable.";
$l['forceportal_exclude_title'] = "Non affected groups";
$l['forceportal_exclude_desc'] = "Choose which groups won't be redirected to the portal";